#include <iostream>
#include "Zombie.hpp"

void	Zombie::announce(void)
{
	std::cout << name << ": BraiiiiiiinnnzzzZ..." << std::endl;
}

void	Zombie::set_name(std::string newname)
{
	name = newname;
}

Zombie::Zombie(std::string newname)
{
	name = newname;
}

Zombie::Zombie(void)
{
	name = "default";
}

Zombie::~Zombie(void)
{
	std::cout << name << " destroyed." << std::endl;
}
