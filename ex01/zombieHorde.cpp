#include "Zombie.hpp"

Zombie	*zombieHorde(int N, std::string name)
{
	Zombie	*horde;

	if (N <= 0)
		return (NULL);
	horde = new Zombie[N];
	while (--N)
	{
		horde[N].set_name(name);
		horde[N].announce();
	}
	horde[N].set_name(name);
	horde[N].announce();
	return (horde);
}
