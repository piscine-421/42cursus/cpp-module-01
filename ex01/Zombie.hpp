#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP
# include <string>

class				Zombie
{
	public:
		void		announce(void);
		void		set_name(std::string newname);
		Zombie(std::string newname);
		Zombie(void);
		~Zombie(void);
	private:
		std::string	name;
};

#endif
