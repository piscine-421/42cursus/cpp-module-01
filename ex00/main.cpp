#include <cstdlib>
#include "Zombie.h"
#include "Zombie.hpp"

int	main(void)
{
	Zombie	*Brian;

	Brian = newZombie("Brian");
	Brian->announce();
	delete Brian;
	randomChump("dude");
	randomChump("guy");
	randomChump("whotf");
	Brian = newZombie("Brian again");
	Brian->announce();
	delete Brian;
	return (EXIT_SUCCESS);
}
