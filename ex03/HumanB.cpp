#include <iostream>
#include "HumanB.hpp"

void	HumanB::attack()
{
	std::cout << name << " attacks with " << weapon->getType() << std::endl;
}

HumanB::HumanB(std::string initname)
{
	name = initname;
}

void	HumanB::setWeapon(Weapon &newweapon)
{
	weapon = &newweapon;
}
