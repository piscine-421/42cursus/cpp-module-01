#include <iostream>
#include "HumanA.hpp"

HumanA::HumanA(std::string initname, Weapon &initWeapon): weapon(initWeapon)
{
	name = initname;
}

void	HumanA::attack()
{
	std::cout << name << " attacks with " << weapon.getType() << std::endl;
}
