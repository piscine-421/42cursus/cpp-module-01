#ifndef WEAPON_HPP
# define WEAPON_HPP
# include <string>

class				Weapon
{
	public:
		std::string	&getType();
		void		 setType(std::string string);
		Weapon(std::string arme);
	private:
		std::string	type;
};

#endif
