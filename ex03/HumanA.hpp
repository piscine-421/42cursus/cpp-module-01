#ifndef HUMANA_HPP
# define HUMANA_HPP
# include "Weapon.hpp"

class				HumanA
{
	public:
		void		attack();
		HumanA(std::string initname, Weapon &initWeapon);
	private:
		std::string	name;
		Weapon		&weapon;
};

#endif
