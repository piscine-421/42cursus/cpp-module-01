#include "Harl.hpp"
#include <iostream>

void	Harl::complain(std::string level)
{
	int			i;
	std::string	names[4] = {"DEBUG", "INFO", "WARNING", "ERROR"};

	for (i = 0; i < 4; i++)
		if (level == names[i])
			break ;
	switch (i)
	{
		case 0:
			debug();
			break ;
		case 1:
			info();
			break ;
		case 2:
			warning();
			break ;
		case 3:
			error();
	}
}

void	Harl::debug(void)
{
	std::cout << "[ DEBUG ]\nI love having extra bacon for my 7XL-double-cheese-triple-pickle-special-ketchup burger. I really do !\n\n";
}

void	Harl::error(void)
{
	std::cout << "[ ERROR ]\nThis is unacceptable ! I want to speak to the manager now.\n\n";
}

void	Harl::info(void)
{
	std::cout << "[ INFO ]\nI cannot believe adding extra bacon costs more money. You didn’t put enough bacon in my burger ! If you did, I wouldn’t be asking for more !\n\n";
}

void	Harl::warning(void)
{
	std::cout << "[ WARNING ]\nI think I deserve to have some extra bacon for free.\nI’ve been coming for years whereas you started working here since last month.\n\n";
}
