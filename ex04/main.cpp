#include <fstream>
#include <iostream>

int	main(int argc, char **argv)
{
	size_t			i;
	std::string		line;
	int				pos;
	std::ofstream	newfile;

	if (argc < 4)
	{
		std::cout << "not enough arguments" << std::endl;
		return (EXIT_FAILURE);
	}
	std::ifstream	file(argv[1]);
	std::string		newfile_name = argv[1];
	std::string		ogstring = argv[2];
	std::string		newstring = argv[3];
	newfile_name.append(".export");
	if (file.is_open())
	{
		newfile.open(newfile_name);
		while (getline(file, line))
		{
			i = line.find(ogstring);
			pos = i;
			while (i != std::string::npos)
			{
				line.erase(i, ogstring.length());
				line.insert(i, newstring);
				pos += newstring.length();
				i = line.find(ogstring, pos);
			}
			newfile << line << std::endl;
		}
	}
	else
	{
		std::cout << "file not found" << std::endl;
		newfile.close();
		file.close();
		return (EXIT_FAILURE);
	}
	newfile.close();
	file.close();
	return (EXIT_SUCCESS);
}
